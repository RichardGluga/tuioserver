﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tuio;
using System.Drawing;

namespace Tuio
{
    class TuioBlob : TuioCursor
    {
        public PointF Dimentions { get; set; }

        public TuioBlob(int id, PointF location, PointF dimentions)
            : base(id, location)
        {
            Dimentions = dimentions;
        }

    }
}
