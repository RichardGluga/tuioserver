﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Tuio
{
    /// <summary>
    /// TUIO object.
    /// 
    /// (c) 2010 by Dominik Schmidt (schmidtd@comp.lancs.ac.uk)
    /// </summary>
    public class TuioObject
    {
        #region properties

        public int Id { get; private set; }

        public PointF Location { get; set; }

        public float Angle { get; set; }

        public PointF Speed { get; set; }

        public float MotionAcceleration { get; set; }

        #endregion

        #region constructors

        public TuioObject(int id, PointF location, float angle)
        {
            Id = id;
            Location = location;
            Angle = angle;
        }

        #endregion

    }
}
