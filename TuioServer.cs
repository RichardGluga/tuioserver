﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;

using Bespoke.Common.Osc;
using SimpleLogger;

namespace Tuio
{
    /// <summary>
    /// Simple, still uncomplete implementation of a TUIO server in C#.
    /// 
    /// Current shortcomings:
    /// Does not implement frame times.
    /// Always commits all cursors.
    /// 
    /// (c) 2010 by Dominik Schmidt (schmidtd@comp.lancs.ac.uk)
    /// (c) 2011 by Chris Ackad (cack7320@uni.sydney.edu.au)
    /// (c) 2012 by Kaz Grace (kazjon@arch.usyd.edu.au)
    /// </summary>
    public class TuioServer
    {
        #region constants
        
        private const string _cursorAddressPattern = "/tuio/2Dcur";

        private const string _blobAddressPattern = "/tuio/2Dblb";

        private const string _objectAddressPattern = "/tuio/2Dobj";

        private const int threshold = 3000;

        private const int lowerbound = 0;

        #endregion

        #region fields

        private IPEndPoint _ipEndPoint;

        private Dictionary<int, TuioCursor> _cursors;

        private Dictionary<int, TuioBlob> _blobs;

        private Dictionary<int, TuioObject> _objects;

        private int _currentFrame;

        #endregion

        #region constructors

        /// <summary>
        /// Creates a new server with and endpoint at localhost, port 3333.
        /// </summary>
        public TuioServer() : this("127.0.0.1", 3333) { }

        /// <summary>
        /// Creates a new server.
        /// </summary>
        /// <param name="host">Endpoint host</param>
        /// <param name="port">Endpoint port</param>
        public TuioServer(string host, int port)
        {
            _cursors = new Dictionary<int, TuioCursor>();
            _blobs = new Dictionary<int, TuioBlob>();
            _objects = new Dictionary<int, TuioObject>();
            _ipEndPoint = new IPEndPoint(IPAddress.Parse(host), port);
            _currentFrame = 0;
        }

        #endregion

        #region frame related methods

        /// <summary>
        /// Initialized a new frame and increases the frame counter.
        /// </summary>
        public void InitFrame()
        {
            _currentFrame++;
        }

        /// <summary>
        /// Commits the current frame.
        /// </summary>
        public void CommitFrame()
        {
            GetFrameBundle().Send(_ipEndPoint);
        }

        #endregion

        #region cursor related methods
        
        /// <summary>
        /// Adds a TUIO cursor. A new id, not used before, must be provided.
        /// </summary>
        /// <param name="id">New id</param>
        /// <param name="location">Location</param>
        public TuioCursor AddTuioCursor(int id, PointF location, PointF dimentions)
        {
            if ((dimentions.X * 1440) < lowerbound || (dimentions.Y * 900) < lowerbound)
            {
                return null;
            }

            if ((dimentions.X * 1440) * (dimentions.Y * 900) < threshold)
            {
                TuioCursor t = new TuioCursor(id, location);
                lock (_cursors)
                    if (!_cursors.ContainsKey(id))
                        _cursors.Add(id, t);
                lock (_blobs)
                    _blobs.Remove(id);
                return t;

            }
            else
            {
                TuioBlob b = new TuioBlob(id, location, dimentions);
                lock (_blobs)
                    if (!_blobs.ContainsKey(id))
                        _blobs.Add(id, b);
                lock (_cursors)
                    _cursors.Remove(id);
                return b;
            }
        }

        /// <summary>
        /// Updates a TUIO cursor. An id of an existing cursor must be provided.
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="location">Location</param>
        public void UpdateTuioCursor(int id, PointF location, PointF dimentions)
        {
            if ((dimentions.X * 1440) < lowerbound || (dimentions.Y * 900) < lowerbound)
            {
                DeleteTuioCursor(id);
                return;
            }


            if ((dimentions.X * 1440) * (dimentions.Y * 900) < threshold)
            {

                lock (_cursors)
                    if (!_cursors.ContainsKey(id))
                        _cursors.Add(id, new TuioCursor(id, location));
                lock (_blobs)
                    _blobs.Remove(id);

                TuioCursor cursor;
                if (_cursors.TryGetValue(id, out cursor))
                    cursor.Location = location;

            }
            else
            {
                lock (_blobs)
                    if (!_blobs.ContainsKey(id))
                        _blobs.Add(id, new TuioBlob(id, location,dimentions));
                lock (_cursors)
                    _cursors.Remove(id);

                TuioBlob blob;
                if (_blobs.TryGetValue(id, out blob))
                    blob.Location = location;
                    blob.Dimentions = dimentions;
            }
        }

        /// <summary>
        /// Deletes a TUIO cursor. An id of an existing cursor must be provided.
        /// </summary>
        /// <param name="id">Id</param>
        public void DeleteTuioCursor(int id)
        {
            lock (_cursors)
                _cursors.Remove(id);
            lock (_blobs)
                _blobs.Remove(id);
        }

        #endregion

        #region object related methods

        /// <summary>
        /// Adds a TUIO object. A new id, not used before, must be provided.
        /// </summary>
        /// <param name="id">New id</param>
        /// <param name="location">Location</param>
        /// <param name="angle">Angle</param>
        public TuioObject AddTuioObject(int id, PointF location, float angle)
        {
            lock (_objects)
                if (!_objects.ContainsKey(id))
                {
                    TuioObject t = new TuioObject(id, location, angle);
                    _objects.Add(id, t);
                    return t;
                }
            return null;
        }

        /// <summary>
        /// Updates a TUIO cursor. An id of an existing cursor must be provided.
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="location">Location</param>
        public void UpdateTuioObject(int id, PointF location, float angle)
        {
            if (location.X == -1 && location.Y == -1) {
                Log.info("ZZZ tuio, id=" + id + " ,location=" + location.ToString() + ", angle=" + angle);
            }
            TuioObject obj;
            lock (_objects)
                if (_objects.TryGetValue(id, out obj))
                {
                    obj.Location = location;
                    obj.Angle = angle;
                }
        }

        /// <summary>
        /// Deletes a TUIO cursor. An id of an existing cursor must be provided.
        /// </summary>
        /// <param name="id">Id</param>
        public void DeleteTuioObject(int id)
        {
            lock (_objects)
                _objects.Remove(id);
        }

        #endregion

        #region osc message assembly

        private OscBundle GetFrameBundle()
        {
            OscBundle bundle = new OscBundle(_ipEndPoint);

            bundle.Append(GetAliveObjMessage());
            foreach (OscMessage msg in GetObjectMessages())
                bundle.Append(msg);
            bundle.Append(GetSequenceObjMessage());
            
            /*bundle.Append(GetAliveMessage());
            foreach (OscMessage msg in GetCursorMessages())
                bundle.Append(msg);
            bundle.Append(GetSequenceMessage());

            bundle.Append(GetAliveBlobMessage());
            foreach (OscMessage msg in GetBlobMessages())
                bundle.Append(msg);
            bundle.Append(GetSequenceBlobMessage());

            */
            //System.Console.WriteLine(bundle.ToByteArray().Length);
            return bundle;
        }

        private OscMessage GetSequenceBlobMessage()
        {
            OscMessage msg = new OscMessage(_ipEndPoint, _blobAddressPattern);

            msg.Append("fseq");
            msg.Append((Int32)_currentFrame);

            return msg;
        }

        private OscMessage GetAliveBlobMessage()
        {
            OscMessage msg = new OscMessage(_ipEndPoint, _blobAddressPattern);

            msg.Append("alive");
            lock (_blobs)
                foreach (TuioBlob blob in _blobs.Values)
                    msg.Append((Int32)blob.Id);

            return msg;
        }

        private OscMessage GetAliveObjMessage()
        {
            OscMessage msg = new OscMessage(_ipEndPoint, _objectAddressPattern);

            msg.Append("alive");
            lock (_objects)
                foreach (TuioObject obj in _objects.Values)
                    msg.Append((Int32)obj.Id);

            return msg;
        }

        private OscMessage GetSequenceObjMessage()
        {
            OscMessage msg = new OscMessage(_ipEndPoint, _objectAddressPattern);

            msg.Append("fseq");
            msg.Append((Int32)_currentFrame);

            return msg;
        }


        private OscMessage GetAliveMessage()
        {
            OscMessage msg = new OscMessage(_ipEndPoint, _cursorAddressPattern);

            msg.Append("alive");
            lock (_cursors)
                foreach (TuioCursor cursor in _cursors.Values)
                    msg.Append((Int32)cursor.Id);

            return msg;
        }

        private OscMessage GetSequenceMessage()
        {
            OscMessage msg = new OscMessage(_ipEndPoint, _cursorAddressPattern);

            msg.Append("fseq");
            msg.Append((Int32)_currentFrame);

            return msg;
        }

        private OscMessage GetCursorMessage(TuioCursor cursor)
        {
            OscMessage msg = new OscMessage(_ipEndPoint, _cursorAddressPattern);

            msg.Append("set");
            msg.Append((Int32)cursor.Id);
            msg.Append(cursor.Location.X);
            msg.Append(cursor.Location.Y);
            msg.Append(cursor.Speed.X);
            msg.Append(cursor.Speed.Y);
            msg.Append(cursor.MotionAcceleration);

            return msg;
        }

        private OscMessage GetBlobMessage(TuioBlob blob)
        {
            OscMessage msg = new OscMessage(_ipEndPoint, _blobAddressPattern);

            msg.Append("set");
            msg.Append((Int32)blob.Id);
            //msg.Append(0f);
            msg.Append(blob.Location.X);
            msg.Append(blob.Location.Y);
            msg.Append(0f); //Angle
            msg.Append(blob.Dimentions.X);
            msg.Append(blob.Dimentions.Y);
            msg.Append(blob.Dimentions.X * blob.Dimentions.Y);
            msg.Append(blob.Speed.X);
            msg.Append(blob.Speed.Y);
            msg.Append(0f); //Rotation vec
            msg.Append(blob.MotionAcceleration);
            msg.Append(0f); //Rotation Accl
            return msg;
        }

        private OscMessage GetObjectMessage(TuioObject obj)
        {
            OscMessage msg = new OscMessage(_ipEndPoint, _objectAddressPattern);

            msg.Append("set");
            msg.Append((Int32)obj.Id);
            msg.Append((Int32)obj.Id);
            //msg.Append(0f);
            msg.Append(obj.Location.X);
            msg.Append(obj.Location.Y);
            msg.Append(obj.Angle); //Angle
            msg.Append(obj.Speed.X);
            msg.Append(obj.Speed.Y);
            msg.Append(0f); //Rotation vec
            msg.Append(obj.MotionAcceleration);
            msg.Append(0f); //Rotation Accl
            return msg;
        }

        private IEnumerable<OscMessage> GetBlobMessages()
        {
            List<OscMessage> msgs = new List<OscMessage>();

            lock (_blobs)
                foreach (TuioBlob blob in _blobs.Values)
                    msgs.Add(GetBlobMessage(blob));

            return msgs.AsEnumerable();
        }

        private IEnumerable<OscMessage> GetCursorMessages()
        {
            List<OscMessage> msgs = new List<OscMessage>();

            lock (_cursors)
                foreach (TuioCursor cursor in _cursors.Values)
                    msgs.Add(GetCursorMessage(cursor));

            return msgs.AsEnumerable();
        }

        private IEnumerable<OscMessage> GetObjectMessages()
        {
            List<OscMessage> msgs = new List<OscMessage>();

            lock (_objects)
                foreach (TuioObject obj in _objects.Values)
                    msgs.Add(GetObjectMessage(obj));

            return msgs.AsEnumerable();
        }

        #endregion
    }
}
